# Generated by Django 4.1 on 2022-08-24 22:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0005_alter_step_recipe_ingredient"),
    ]

    operations = [
        migrations.AddField(
            model_name="ingredient",
            name="amount",
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name="ingredient",
            name="recipe",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="ingredients",
                to="recipes.recipe",
            ),
        ),
    ]
